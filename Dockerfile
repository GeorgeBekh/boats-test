FROM golang:1.13-alpine

WORKDIR /go/src/gitlab.com/GeorgeBekh/boat-test/

COPY . .

RUN go build -o app .

FROM alpine

WORKDIR /app

COPY ./dist ./dist

COPY --from=0 /go/src/gitlab.com/GeorgeBekh/boat-test/app .
CMD ["./app"]
